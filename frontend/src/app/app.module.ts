import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ENV, environment } from '~env';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {
      useValue: environment,
      provide: ENV,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
