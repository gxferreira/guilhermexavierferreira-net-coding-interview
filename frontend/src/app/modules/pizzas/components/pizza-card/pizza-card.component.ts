import { Component, Input, OnInit } from '@angular/core';
import { ReplaySubject } from "rxjs";
import { PizzaModel } from "~app/core/state/pizza.model";
import { PizzasService } from "~app/core/services/pizzas/pizzas.service";
import { switchMap, take } from "rxjs/operators";

@Component({
  selector: 'app-pizza-card',
  templateUrl: './pizza-card.component.html',
  styleUrls: ['./pizza-card.component.scss']
})
export class PizzaCardComponent implements OnInit {
  pizza$: ReplaySubject<PizzaModel> = new ReplaySubject<PizzaModel>();
  @Input() set pizza(data: PizzaModel) {
    this.pizza$.next(data);
  };

  constructor(
    private readonly pizzasService: PizzasService
  ) { }

  ngOnInit(): void {
  }

  activatePizza(): void {
    this.pizza$
      .asObservable()
      .pipe(take<PizzaModel>(1))
      .pipe(switchMap(pizza => this.pizzasService.activatePizza(pizza._id)))
      .subscribe();
  }

  disablePizza(): void {
    this.pizza$
      .asObservable()
      .pipe(take<PizzaModel>(1))
      .pipe(switchMap(pizza => this.pizzasService.disablePizza(pizza._id)))
      .subscribe();
  }

}
