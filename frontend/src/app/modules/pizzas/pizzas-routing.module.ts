import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PizzasComponent } from "./pages/pizzas/pizzas.component";
import { PizzaComponent } from "./pages/pizza/pizza.component";

const routes: Routes = [
  {
    path: '',
    component: PizzasComponent,
  },
  {
    path: ':pizzaId',
    component: PizzaComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PizzasRoutingModule { }
