import { Component, OnInit } from '@angular/core';
import { PizzasStore } from "~app/core/state/pizzas.store";
import { PizzasService } from "~app/core/services/pizzas/pizzas.service";

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.scss']
})
export class PizzasComponent implements OnInit {
  pizzas$ = this.pizzasService.pizzas$;

  constructor(
    private readonly pizzasService: PizzasService,
  ) { }

  ngOnInit(): void {
    this.pizzasService.getPizzas()
      .subscribe();
  }

}
