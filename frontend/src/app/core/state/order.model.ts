export interface OrderModel {
  _id: string;
  status: 'Waiting',

  pizzas: Array<{
    pizzaId: string;
    extraIngredients: Array<{
      ingredientId: string;
      name: string;
      price: number;
    }>
    basePrice: number;
    extraPrice: number;
    endPrice: number;
  }>

  subTotal: number;
  total: number;
}
