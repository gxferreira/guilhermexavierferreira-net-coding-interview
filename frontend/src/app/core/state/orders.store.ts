import { Injectable } from '@angular/core';
import { BaseStore } from "~app/core/state/base.store";
import { OrderModel } from "~app/core/state/order.model";

const initialState = {
  requestingOrders: false,
};

@Injectable({
  providedIn: 'root'
})
export class OrdersStore extends BaseStore<typeof initialState, OrderModel> {

  constructor() {
    super(initialState)
  }
}
