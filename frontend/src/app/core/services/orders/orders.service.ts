import { Inject, Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { OrderModel } from "~app/core/state/order.model";
import { ENV, environment } from "~env";
import { HttpClient } from "@angular/common/http";
import { OrdersStore } from "~app/core/state/orders.store";
import { map, pluck } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  orders$: Observable<OrderModel[]> = this.ordersStore.find();

  constructor(
    @Inject(ENV)
    private readonly env: typeof environment,
    private readonly http: HttpClient,
    private readonly ordersStore: OrdersStore,
  ) { }

  getOrders(): Observable<OrderModel[]> {
    this.ordersStore.update(state => ({ ...state, requestingOrders: true }));
    return this.http.get<{ orders: OrderModel[] }>(`${this.env.api}/orders`)
      .pipe(pluck('orders'))
      .pipe(map(orders => {
        this.ordersStore.add(orders);
        this.ordersStore.update(state => ({ ...state, requestingOrders: false }));

        return orders;
      }))
  }
}
