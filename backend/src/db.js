const mongoose = require('mongoose');

exports.connectDB = () => new Promise((resolve, reject) => {
  mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
  const db = mongoose.connection;
  db.on('error', reject);
  db.once('open', resolve);
});
