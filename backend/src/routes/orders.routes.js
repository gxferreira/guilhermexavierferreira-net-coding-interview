const express = require("express");
const mongoose = require('mongoose');
const { Order } = require('../models/order.model');
const { Pizza } = require("../models/pizza.model");

exports.setOrdersRoutes = (app, basePath) => {
  const router = express.Router();

  router.get('/', async (request, response) => {
    const orders = await Order.find().lean().exec();

    return response.send({ orders: orders });
  });

  router.post('/', async (request, response) => {
    const { body } = request;
    const pizzas = await loadPizzas(body);

    const order = createOrder();
    order.pizzas = pizzas.map(
      fromPizzaToPizzaItem
    );
    order.total = order.pizzas.reduce(
      (acc, p) => (acc + p.basePrice), 0
    )
    order.subTotal = order.total;

    const newOrder = new Order(order);
    await newOrder.save();

    response.status(200).send(
      { order: newOrder }
    );
  })

  app.use(basePath, router);
};

function orderIsAvailabe(pizzas, extraIngredients) {

}

async function loadPizzas(payload) {
  const $in = payload.map(
    ({id}) => {
      return id 
    }
  );

  const query = {
    '_id': { $in }
  }

  return Pizza.find(query).lean().exec();
}

function createOrder() {
  const status = "Waiting";
  const pizzas = []

  return {
    status,
    pizzas,
    subTotal: 0,
    total: 0
  }
}

function fromPizzaToPizzaItem(pizza) {
  const { basePrice } = pizza;
  const pizzaId = pizza._id;
  const endPrice = basePrice;
  const extraPrice = 0;

  return { basePrice, pizzaId, endPrice, extraPrice };
}
