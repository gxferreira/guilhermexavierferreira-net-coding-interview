const express = require("express");
const { Pizza } = require('../models/pizza.model');

exports.setPizzaRoutes = (app, basePath) => {
  const router = express.Router();

  router.get('/', async (request, response) => {
    const pizzas = await Pizza.find().lean().exec();
    response.status(200).send({ pizzas });
  });

  router.post('/', async (request, response) => {
    const { pizza } = request.body;
    const newPizza = new Pizza(pizza);

    await newPizza.save();

    response.status(200).send({ pizza: newPizza });
  });

  router.get('/:pizzaId', async (request, response) => {
    const { pizzaId } = request.params;
    const pizza = await Pizza.findById(pizzaId).lean().exec();

    response.status(200).send({ pizza });
  });

  router.put('/:pizzaId/activate', async (request, response) => {
    const { pizzaId } = request.params;

    const updatedPizza = await Pizza.findByIdAndUpdate(pizzaId, {
      isAvailable: true,
    }, { new: true }).lean().exec();

    response.status(200).send({
      pizza: updatedPizza
    });
  });

  router.put('/:pizzaId/disable', async (request, response) => {
    const { pizzaId } = request.params;

    const updatedPizza = await Pizza.findByIdAndUpdate(pizzaId, {
      isAvailable: false,
    }, { new: true }).lean().exec();

    response.status(200).send({
      pizza: updatedPizza
    });
  });

  app.use(basePath, router);
};
