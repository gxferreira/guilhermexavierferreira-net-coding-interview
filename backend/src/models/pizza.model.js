const mongoose = require('mongoose');

const PizzaScheme = new mongoose.Schema({
  name: { type: String, required: true },
  baseIngredients: {
    type: [new mongoose.Schema({
      ingredientId: { type: mongoose.Types.ObjectId, required: true },
      name: { type: String, required: true },
    }, { _id: false, timestamps: false })],
    required: true
  },
  isAvailable: { type: Boolean, required: true },
  basePrice: { type: Number, required: true },
  cheeseBorder: { type: Boolean, required: true },
}, { timestamps: true, collection: 'pizzas' });

exports.Pizza = mongoose.model('Pizza', PizzaScheme);
